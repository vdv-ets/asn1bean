plugins {
    antlr
    java
}

spotless {
    java {
        targetExclude(fileTree("$buildDir") { include("**/*.java") })
    }
}

dependencies {
    implementation("antlr:antlr:2.7.7")
    implementation(project(":asn1bean-runtime"))
}

val generatedAsn1BeanDir = layout.buildDirectory.dir("generated-src/asn1bean-compiler/test").get()

val filesToConfig = mapOf(
    setOf("tagging-test.asn") to arrayOf("-dv"),
    setOf("compile-test.asn") to arrayOf("-dv", "-l"),
    setOf("modules1.asn", "modules2.asn") to arrayOf("-dv"),
    setOf("iso-presentation-layer.asn") to arrayOf("-dv", "-l"),
    setOf("x690-ber-example.asn") to arrayOf("-dv"),
    setOf("mobile/PKIXExplicit88.asn", "mobile/PKIXImplicit88.asn", "mobile/RSPDefinitionsV2.2.asn") to arrayOf("-dv", "-l"),
    setOf("mobile/PEDefinitionsV2.3.1.asn") to arrayOf("-dv", "-l"),
    setOf("various-tests.asn") to arrayOf("-dv", "-l"),
    setOf("information-object-class.asn") to arrayOf("-dv", "-l"),
    setOf("extension-test1.asn") to arrayOf("-dv"),
    setOf("extension-test2.asn") to arrayOf("-dv", "-e"),
)

tasks.register("generateConfiguredAsn1Beans") {
    dependsOn("compileJava")

    inputs.files(sourceSets["test"].resources)
    outputs.dir(generatedAsn1BeanDir)

    val taskOutput = StringBuilder()
    logging.addStandardErrorListener { taskOutput.append(it) }

    filesToConfig.forEach { (files, params) ->
        doLast {
            try {
                javaexec {
                    jvmArgs("-Dfile.encoding=UTF8")
                    classpath = sourceSets["main"].runtimeClasspath
                    mainClass.set("com.beanit.asn1bean.compiler.Compiler")
                    args("-p")
                    args("com.beanit.asn1bean.compiler")
                    args("-o")
                    args(generatedAsn1BeanDir)
                    args("-f")
                    files.forEach { args("src/test/resources/" + it) }
                    params.forEach { args(it) }
                }
            } catch (e: Exception) {
                throw RuntimeException("Error generating ASN.1 file using asn1bean: ${e.message}.", e)
            }
            if (taskOutput.toString().length != 0) {
                throw RuntimeException("Error generating ASN.1 file using asn1bean")
            }
        }
    }
}

tasks.named("compileTestJava") {
    dependsOn("generateConfiguredAsn1Beans")
}

java.sourceSets["test"].java {
    srcDir(generatedAsn1BeanDir)
}

project.extra["cfgModuleName"] = "com.beanit.asn1bean-compiler"

tasks["jar"].withConvention(aQute.bnd.gradle.BundleTaskConvention::class) {
    bnd("""
        Bundle-Name: ASN1bean Compiler
        Bundle-SymbolicName: ${project.extra["cfgModuleName"]}
        -exportcontents: !*.internal.*,*
    """)
}

publishing {
    publications {
        maybeCreate<MavenPublication>("mavenJava").pom {
            name.set("ASN1bean Compiler")
            description.set("ASN1bean Compiler generates Java classes out of ASN.1 code that can be used to encode/decode BER data.")

            licenses {
                license {
                    name.set("The Apache License, Version 2.0")
                    url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                }
            }
        }
    }
}
