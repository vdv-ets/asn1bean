rootProject.name = "asn1bean"

include("asn1bean-compiler")
include("asn1bean-runtime")

project(":asn1bean-compiler").projectDir = file("projects/asn1bean-compiler")
project(":asn1bean-runtime").projectDir = file("projects/asn1bean-runtime")
